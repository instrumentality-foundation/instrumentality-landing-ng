import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'card-comp',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.less']
})
export class CardComponent implements OnInit {

  @Input() imageLocation: string;
  @Input() bgLocation: string;
  @Input() cardTitle: string;
  @Input() cardContent: string;

  constructor() { }

  ngOnInit(): void {
  }

}
