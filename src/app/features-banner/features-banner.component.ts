import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'features-banner-comp',
  templateUrl: './features-banner.component.html',
  styleUrls: ['./features-banner.component.less']
})
export class FeaturesBannerComponent implements OnInit {

  @Input() background: string;
  @Input() title: string;

  constructor() { }

  ngOnInit(): void {
  }

}
