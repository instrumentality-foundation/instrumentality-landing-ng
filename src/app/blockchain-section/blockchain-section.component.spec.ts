import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockchainSectionComponent } from './blockchain-section.component';

describe('BlockchainSectionComponent', () => {
  let component: BlockchainSectionComponent;
  let fixture: ComponentFixture<BlockchainSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockchainSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockchainSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
