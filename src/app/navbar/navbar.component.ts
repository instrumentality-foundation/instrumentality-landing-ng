import { Component, OnInit } from '@angular/core';

// Importing font-awesome icons
import { faBars } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'navbar-comp',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.less']
})
export class NavbarComponent implements OnInit {

  faBars = faBars;

  constructor() { }

  ngOnInit(): void {
  }

}
