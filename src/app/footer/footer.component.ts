import { Component, OnInit } from '@angular/core';

// Importing icons
import { faAngular } from "@fortawesome/free-brands-svg-icons";
import { faUikit } from "@fortawesome/free-brands-svg-icons";
import { faLess } from "@fortawesome/free-brands-svg-icons";
import { faFontAwesome } from "@fortawesome/free-brands-svg-icons";

@Component({
  selector: 'footer-comp',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.less']
})
export class FooterComponent implements OnInit {

  faAngular = faAngular;
  faUikit = faUikit;
  faLess = faLess;
  faFontAwesome = faFontAwesome;

  constructor() { }

  ngOnInit(): void {
  }

}
